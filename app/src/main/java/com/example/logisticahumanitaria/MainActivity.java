package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.DbEstados;
import database.Device;
import database.Estados;
import database.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject> ,Response.ErrorListener{
    private EditText edtNombre;
    private Estados savedEstado;
    private int id;
    ProcesosPHP php;
    DbEstados agenda;
    JsonObjectRequest jsonObjectRequest;
    private String serverip = "https://estadosequipo4.000webhostapp.com/WebService/";
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private Button btnCerrar;
    SharedPreferences sharedPreferences;
    private RequestQueue request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.php =new ProcesosPHP();
        php.setContext(this);
        this.request = Volley.newRequestQueue(this);
        this.agenda=new DbEstados(this);
        setContentView(R.layout.activity_main);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar =(Button) findViewById(R.id.btnCerrar);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        sharedPreferences = getSharedPreferences("datos", Context.MODE_PRIVATE);

        boolean agregados = sharedPreferences.getBoolean("agregados",false);
        if(!agregados) {
            if(isNetworkAvailable()) {
                this.mostrarTodosWebService(Device.getSecureId(MainActivity.this));
                SharedPreferences.Editor edPrim = sharedPreferences.edit();
                edPrim.putBoolean("agregados", false);
                edPrim.commit();
            }
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isNetworkAvailable()){
                    boolean completo = true;
                    if (edtNombre.getText().toString().equals("")) {
                        edtNombre.setError("Introduce el nombre");
                        completo = false;
                    }
                    if (completo) {
                        final Estados nEstado = new Estados();
                        nEstado.setNombre(edtNombre.getText().toString());
                        nEstado.setIdMovil(Device.getSecureId(MainActivity.this));

                        if (savedEstado == null) {
                            php.insertarEstadoWebService(nEstado);
                            agenda.openDatabase();
                            agenda.insertEstado(nEstado);
                            agenda.close();
                            Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        else{
                            php.actualizarEstadoWebService(nEstado,id);
                            agenda.openDatabase();
                            agenda.updateEstado(nEstado,id);
                            agenda.close();
                            Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                }
                else
                    Toast.makeText(MainActivity.this, R.string.conexion, Toast.LENGTH_SHORT).show();
            }
        });


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(i, 0);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    public void mostrarTodosWebService(String idMovil){
        String url = serverip + "wsConsultarTodos.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estados estado = (Estados) data.getSerializableExtra("estados");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
        }
        else
            limpiar();
    }

    public void limpiar(){
        savedEstado = null;
        edtNombre.setText("");
    }

    @Override
    public void onErrorResponse(VolleyError error) {}

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("estados");
        try {
            DbEstados agenda = new DbEstados(MainActivity.this);
            agenda.openDatabase();
            Estados estado=null;
            if(json==null)
                return;
            for (int i = 0;i < json.length(); i++) {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                agenda.insertEstado(estado);
            }
            agenda.close();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("agregados", true);
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}