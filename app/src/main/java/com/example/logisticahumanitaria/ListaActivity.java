package com.example.logisticahumanitaria;


import android.app.Activity;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.DbEstados;
import database.DefinirTabla;
import database.Device;
import database.Estados;
import database.ProcesosPHP;

public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>,Response.ErrorListener{

    private final Context context = this;
    private DbEstados estadosPersistencia = new DbEstados(ListaActivity.this);
    ProcesosPHP php;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> listaContactos;
    SharedPreferences sharedPreferences;
    private String serverip = "https://estadosequipo4.000webhostapp.com/WebService/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        this.php=new ProcesosPHP();
        php.setContext(this);
        Button btnNuevo = findViewById(R.id.btnNuevo);

        if(isNetworkAvailable())
        {
            listaContactos = new ArrayList<Estados>();
            request = Volley.newRequestQueue(context);
            php.mostrarContactosWebService(context);
            mostrarTodosWebService(Device.getSecureId(ListaActivity.this));
        }else{
            estadosPersistencia = new DbEstados(ListaActivity.this);
            estadosPersistencia.openDatabase();
            ArrayList<Estados> estados = estadosPersistencia.allContactos();
            MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.activity_estado, estados);
            setListAdapter(adapter);
       }

        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

   public void mostrarTodosWebService(String idMovil){
        String url = serverip + "wsConsultarTodos.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        this.listaContactos.removeAll(this.listaContactos);
        Estados estado = null;
        JSONArray json = response.optJSONArray("estados");

        try {

            for (int i = 0;i < json.length(); i++) {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                listaContactos.add(estado);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(context, R.layout.activity_estado, listaContactos);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = view.findViewById(R.id.lblNombreEstado);
            Button modificar = view.findViewById(R.id.btnModificar);
            Button borrar = view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombre());

            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(isNetworkAvailable()){
                        php.setContext(context);
                        php.borrarEstadoWebService(objects.get(position).get_ID());
                        estadosPersistencia.openDatabase();
                        estadosPersistencia.deleteEstado(objects.get(position).get_ID());
                        estadosPersistencia.close();
                        objects.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), R.string.eliminar,Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(getApplicationContext(), R.string.conexion, Toast.LENGTH_SHORT).show();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isNetworkAvailable()){
                        Bundle oBundle = new Bundle();
                        oBundle.putSerializable("estados", objects.get(position));
                        Intent i = new Intent();
                        i.putExtras(oBundle);
                        setResult(Activity.RESULT_OK, i);
                        finish();
                    }
                    else
                        Toast.makeText(getApplicationContext(), R.string.conexion, Toast.LENGTH_SHORT).show();

                }
            });
            return view;
        }
    }
}