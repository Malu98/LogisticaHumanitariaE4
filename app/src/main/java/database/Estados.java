package database;

import java.io.Serializable;

public class Estados implements Serializable {
    private int _ID;
    private String nombre;
    private String idMovil;
    public Estados(){
        this.set_ID(0);
        this.setNombre("");
        this.setIdMovil("");

    }
    public Estados(int _ID, String nombre, String idMovil){
        this.set_ID(_ID);
        this.setNombre(nombre);
        this.setIdMovil(idMovil);

    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(String idMovil) {
        this.idMovil = idMovil;
    }
}

