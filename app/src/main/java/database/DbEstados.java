package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {

    Context context;
    EstadoDbHelper eDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE,
            DefinirTabla.Estado.COLUMN_NAME_IDMOVIL
    };

    public DbEstados(Context context){
        this.context = context;
        this.eDbHelper = new EstadoDbHelper(this.context);
    }

    public void openDatabase(){
        db = eDbHelper.getWritableDatabase();
    }

    public long insertEstado(Estados es){
        ContentValues values = new ContentValues();
       // values.put(DefinirTabla.Estado.COLUMN_NAME_ID, es.get_ID());
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, es.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, es.getIdMovil());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
    }

    public long updateEstado(Estados es, int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, es.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, es.getIdMovil());
        return db.update(DefinirTabla.Estado.TABLE_NAME, values, DefinirTabla.Estado._ID + " = " + id, null);
    }

    public int deleteEstado(long id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME, DefinirTabla.Estado._ID + "=?", new String[]{ String.valueOf(id) });
    }

    public int deleteEstadoAll(){
        return db.delete(DefinirTabla.Estado.TABLE_NAME, null, null);
    }

    private Estados readEstado(Cursor cursor){
        Estados es = new Estados();
        es.set_ID(cursor.getInt(0));
        es.setNombre(cursor.getString(1));
        es.setIdMovil(cursor.getString(2));
        return es;
    }

    public Estados getContacto(long id){
        SQLiteDatabase db = eDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME, columnsToRead, DefinirTabla.Estado._ID + " = ?", new String[] {String.valueOf(id)}, null, null, null);
        c.moveToFirst();
        Estados estado = readEstado(c);
        c.close();
        return estado;
    }

    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME, columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readEstado(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void close(){
        eDbHelper.close();
    }
}
