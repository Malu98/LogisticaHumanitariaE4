package database;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener{
    private boolean consulta;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();
    private String serverip = "https://estadosequipo4.000webhostapp.com/WebService/";

    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }

    public void insertarEstadoWebService(Estados es){
//        this.consulta=false;
        String url = serverip + "wsRegistro.php?nombre="+es.getNombre() +"&idMovil="+es.getIdMovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void actualizarEstadoWebService(Estados es,int id){
        //this.consulta=false;
        String url = serverip + "wsActualizar.php?_ID="+id+"&nombre="+es.getNombre();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void borrarEstadoWebService(int id){
       // this.consulta=false;
        String url = serverip + "wsEliminar.php?_ID="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }


    public ArrayList<Estados> mostrarContactosWebService(Context context)
    {
        //this.consulta=true;
        String url = serverip + "wsConsultarTodos.php?idMovil="+Device.getSecureId(context);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    public ArrayList<Estados> mostrarContactoWebService(int idMovil)
    {
       // this.consulta=true;
        String url = serverip + "wsConsultarUno.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {

    }

    @Override
    public void onResponse(JSONObject response) {

          this.contactos.removeAll(this.contactos);
            Estados contacto = null;
            JSONArray json = response.optJSONArray("estados");
            try {
                for (int i = 0;json!=null && i < json.length(); i++) {
                    contacto = new Estados();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    contacto.set_ID(jsonObject.optInt("_ID"));
                    contacto.setNombre(jsonObject.optString("estado"));
                    contacto.setIdMovil(jsonObject.optString("idMovil"));
                    contactos.add(contacto);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

}
